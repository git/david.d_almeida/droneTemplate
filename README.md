[![Build Status](https://codefirst.iut.uca.fr/api/badges/david.d_almeida/droneTemplate/status.svg)](https://codefirst.iut.uca.fr/david.d_almeida/droneTemplate)  
[![Quality Gate Status](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=alert_status)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)
[![Bugs](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=bugs)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)
[![Code Smells](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=code_smells)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)
[![Coverage](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=coverage)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)  
[![Duplicated Lines (%)](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=duplicated_lines_density)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)
[![Lines of Code](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=ncloc)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)
[![Maintainability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=sqale_rating)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)
[![Reliability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=reliability_rating)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)  
[![Security Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=security_rating)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)
[![Technical Debt](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=sqale_index)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)
[![Vulnerabilities](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=droneTemplate&metric=vulnerabilities)](https://codefirst.iut.uca.fr/sonar/dashboard?id=droneTemplate)  

 
# droneTemplate

Welcome on the droneTemplate project!  

  

_Generated with a_ **Code#0** _template_  
<img src="Documentation/doc_images/CodeFirst.png" height=40/>   